#ifndef INTEGRATOR_H
#define INTEGRATOR_H

#include<complex>

namespace itg {

const int INTEGRATION_SUBDIVISIONS = 100;

template <typename T>
double Integrate(double a, double b, T f)
{
    double step = (b - a) / INTEGRATION_SUBDIVISIONS;
    double result(0);
    for(double i = a; i <= b; i+= step)
    {
        result += f(i) * step;
    }
    return result;
}

template <typename T>
std::complex<double> ComplexIntegrate(double a, double b, T f)
{
    double step = (b - a) / INTEGRATION_SUBDIVISIONS;
    std::complex<double> result(0);
    for(double i = a; i <= b; i+= step)
    {
        result += f(i) * step;
    }
    return result;
}

}

#endif // INTEGRATOR_H
