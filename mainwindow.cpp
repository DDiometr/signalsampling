#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setupPlots()
{
    ui->signalPlot->clearGraphs();
    ui->signalPlot->addGraph();
    ui->signalPlot->addGraph();
    ui->signalPlot->setAutoAddPlottableToLegend(true);
    ui->signalPlot->legend->setVisible(true);

    ui->signalPlot->xAxis->setLabel("t, мкс");
    ui->signalPlot->yAxis->setLabel("S, мкВ");

    ui->discPlot->xAxis->setLabel("n");
    ui->discPlot->yAxis->setLabel("Sд, мкВ");

    ui->discPlot->setInteraction(QCP::iRangeDrag, true);
    ui->discPlot->setInteraction(QCP::iRangeZoom, true);

    ui->quantPlot->xAxis->setLabel("n");
    ui->quantPlot->yAxis->setLabel("Sк, мкВ");

    ui->errorPlot->xAxis->setLabel("n");
    ui->errorPlot->yAxis->setLabel("\u0394, мкВ");
    ui->errorPlot->addGraph();
}

void MainWindow::plotData(QCustomPlot &plot, const int graph, const QVector<double> &x, const QVector<double> &y)
{
    plot.graph(graph)->data().clear();
    plot.graph(graph)->setData(x, y);
    plot.rescaleAxes();
    plot.replot();
}

double MainWindow::getAverageErr(const double qStep)
{
    double sum = 0;
    double dx = 1 / qStep;
    for(double i = -qStep / 2; i <= qStep / 2; i += dx)
    {
        sum += i * dx;
    }
    return sum / qStep;
}


void MainWindow::on_pushButton_clicked()
{
    double f = ui->freqInp->text().toDouble();
    double T = ui->periodInput->text().toDouble();
    setupPlots();

    SignalFunction S(f, T);
    QVector<double> signalX, signalY;
    for(double t = 0; t <= T; t += 0.01)
    {
        signalX.push_back(t);
        signalY.push_back(S(t));
    }
    ui->signalPlot->graph(0)->setName("Исходный сигнал");
    plotData(*(ui->signalPlot), 0, signalX, signalY);

    SquaredSignalFunction S2(S);
    double signalEnergy = itg::Integrate(0, T, S2);
    double freqEnergy = 0;
    double edgeOmega = 0;
    ExpSignalFunction Se(S, edgeOmega);
    Sjw sFunc(Se);
    Sjw2 sSquareFunc(sFunc);
    while(freqEnergy / signalEnergy <= 0.98)
    {
        edgeOmega += 1;
        freqEnergy = 1 / M_PI * abs(itg::ComplexIntegrate(0, edgeOmega, sSquareFunc));
    }

    double edgeF = edgeOmega / (2 * M_PI);
    ui->upperOmegaOut->setText(QString::number(edgeF));
    double discF = edgeF * 2;
    ui->discOmegaOut->setText(QString::number(discF));
    double discStep = 1 / discF;
    double N = 1 + T / discStep;
    int leftN = floor(N);
    int rightN = ceil(N);
    if(leftN % 2 != 0) leftN--;
    if(rightN % 2 != 0) rightN++;
    if(abs(N - rightN) <= abs(N - leftN))
    {
        N = rightN;
    }
    else
    {
        N = leftN;
    }
    discStep = T / (N - 1);

    QVector<double> discX, discY;
    int counter = 0;
    for(double i = 0; i <= T; i += discStep)
    {
        discX.push_back(counter);
        discY.push_back(S(i));
        counter++;
    }

    QCPBars *disc = new QCPBars(ui->discPlot->xAxis, ui->discPlot->yAxis);
    disc->addData(discX, discY);

    ui->discPlot->rescaleAxes();
    ui->discPlot->replot();

    ui->signalValuesTable->setRowCount(N - 1);
    ui->signalValuesTable->setColumnCount(2);
    for(int i = 0; i < N - 1; i++)
    {
        ui->signalValuesTable->setItem(i, 0, new QTableWidgetItem(QString::number(discX[i] * discStep)));
        ui->signalValuesTable->setItem(i, 1, new QTableWidgetItem(QString::number(discY[i])));
    }
    ui->signalValuesTable->resizeRowsToContents();

    double sMin = discY[0], sMax = discY[0];
    for (int i = 0; i < discY.count(); i++ )
    {
        if(discY[i] <= sMin) sMin = discY[i];
        if(discY[i] >= sMax) sMax = discY[i];
    }

    double quant = (sMax - sMin) / 15;
    int level = 0;
    for (int i = 0; i < discY.count(); i++ )
    {
        level = round(discY[i] / quant);
        discY[i] = quant * level;
    }

    QCPBars *quantLevel = new QCPBars(ui->quantPlot->xAxis, ui->quantPlot->yAxis);
    quantLevel->addData(discX, discY);
    ui->quantPlot->rescaleAxes();
    ui->quantPlot->replot();

    std::vector<double> qs(discY.begin(), discY.end());
    Recovered Sv(qs, static_cast<double>(N), T);

    QVector<double>recY;
    for(double t = 0; t <= T; t += 0.01)
    {
        recY.push_back(Sv(t));
    }
    QPen p(QColor(0, 255, 0));
    ui->signalPlot->graph(1)->setPen(p);
    ui->signalPlot->graph(1)->setName("Восстановленный сигнал");
    plotData(*(ui->signalPlot), 1, signalX, recY);

    ui->DPFValuesTable->setRowCount(N / 2 - 1);
    ui->DPFValuesTable->setColumnCount(2);
    for(int i = 0; i < N - 1; i++)
    {
        ui->DPFValuesTable->setItem(i, 0, new QTableWidgetItem(QString::number(i)));
        QString rs = QString::number(Sv.coefs()[i].real());
        QString ss = signbit(Sv.coefs()[i].imag()) ? "+" : "-";
        QString is = QString::number(abs(Sv.coefs()[i].imag()));
        QString c = rs + " " + ss + " " + is + "j";
        ui->DPFValuesTable->setItem(i, 1, new QTableWidgetItem(c));
    }
    ui->DPFValuesTable->resizeRowsToContents();

    double ae = getAverageErr(discStep);
    ui->avErrOut->setText(QString::number(ae));
    ui->errDispOut->setText(QString::number(ae * ae / 3));

    QVector<double> errX, errY;
    int ctr = 0;
    for(double i = 0; i <= T; i += discStep)
    {
        errX.push_back(ctr);
        errY.push_back(abs(S(i) - discX[ctr]));
        ctr++;
    }
    plotData(*(ui->errorPlot), 0, errX, errY);
}


void MainWindow::on_copySignalBTN_clicked()
{
    QClipboard *clipboard = QApplication::clipboard();
    QString data = "";

    double rCount = ui->signalValuesTable->rowCount();
    double cCount = ui->signalValuesTable->columnCount();

    for(int i = 0; i < rCount; i++)
    {
        for(int j = 0; j < cCount; j++)
        {
            data.append(ui->signalValuesTable->item(i, j)->text());
            data.append('\t');

        }
        data.append('\n');
    }

    rCount = ui->DPFValuesTable->rowCount();
    cCount = ui->DPFValuesTable->columnCount();

    for(int i = 0; i < rCount; i++)
    {
        for(int j = 0; j < cCount; j++)
        {
            data.append(ui->DPFValuesTable->item(i, j)->text());
            data.append('\t');

        }
        data.append('\n');
    }

    clipboard->setText(data);
}

