#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <cmath>
#include <complex>
#include <vector>

#include "Integrator.h"

class SignalFunction
{
public:
    SignalFunction(double f, double T) : m_f(f), m_T(T)
    {

    }

    double operator() (double x)
    {
        if(x >= 0 && x <= m_T) return cos(2 * M_PI * m_f * x);
        else return 0;
    }

    double T() const;

private:
    double m_f, m_T;
};

inline double SignalFunction::T() const
{
    return m_T;
}

class SquaredSignalFunction
{
public:
    SquaredSignalFunction(SignalFunction &f) : m_function(f)
    {

    }
    double operator() (double x)
    {
        return m_function(x) * m_function(x);
    }
private:
    SignalFunction &m_function;
};

class ExpSignalFunction
{
public:
    ExpSignalFunction(SignalFunction &f, double w) : m_function(f), m_w(w)
    {

    }

    std::complex<double> operator() (double x)
    {
        std::complex<double> temp(cos(m_w * x), -sin(m_w * x));
        return m_function(x) * temp;
    }

    void setW(double newW);

    SignalFunction &function() const;

private:
    SignalFunction &m_function;
    double m_w;
};

inline void ExpSignalFunction::setW(double newW)
{
    m_w = newW;
}

inline SignalFunction &ExpSignalFunction::function() const
{
    return m_function;
}

class Sjw
{
public:
    Sjw(ExpSignalFunction &f) : m_function(f)
    {

    }
    std::complex<double> operator() (double w)
    {
        m_function.setW(w);
        double p = m_function.function().T();
        return itg::ComplexIntegrate(0, p, m_function);
    }
private:
    ExpSignalFunction &m_function;
};

class Sjw2
{
public:
    Sjw2(Sjw &f) : m_function(f)
    {

    }
    std::complex<double> operator() (double w)
    {
        return m_function(w) * m_function(w);
    }
private:
    Sjw &m_function;
};

class Recovered
{
public:
    Recovered(std::vector<double> &qs, int N, double T) : m_N(N), m_T(T)
    {
        for(int n = 0; n < N / 2; n++)
        {
            double temp = 1.0 / N;
            std::complex<double> sum(0);
            for(int k = 0; k < N - 1; k++)
            {
                std::complex<double> t(cos(2 * M_PI * n * k / N), -sin(2 * M_PI * n * k / N));
                sum += qs[k] * t;
            }
            m_coefs.push_back(sum * temp);
        }
    }


    double operator() (double t)
    {
        double sum = m_coefs[0].real();
        for(int n = 1; n < m_coefs.size(); n++)
        {
            sum += 2 * abs(m_coefs[n]) * cos(2 * n * M_PI * t / m_T + arg(m_coefs[n]));
        }
        return sum;
    }
    const std::vector<std::complex<double> > &coefs() const;

private:
    std::vector<std::complex<double>> m_coefs;
    int m_N;
    double m_T;
};

inline const std::vector<std::complex<double> > &Recovered::coefs() const
{
    return m_coefs;
}

#endif // FUNCTIONS_H
