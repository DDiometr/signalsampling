#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <math.h>

#include "Integrator.h"
#include "Functions.h"
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private slots:
    void on_pushButton_clicked();

    void on_copySignalBTN_clicked();

private:
    Ui::MainWindow *ui;

    void setupPlots();
    void plotData(QCustomPlot &plot, const int graph, const QVector<double> &x, const QVector<double> &y);
    double getAverageErr(const double qStep);
};
#endif // MAINWINDOW_H
